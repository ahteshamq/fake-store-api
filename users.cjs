function fetchData() {
    fetch('https://fakestoreapi.com/users')
        .then(response => {
            if (!response.ok) {
                throw Error('Data did not fetch')
            }
            return response.json()
        })
        .then(data => {
            console.log(data)
            let ring = document.getElementsByClassName('lds-ring');
            ring[0].style.display = 'none';


            let count = `<span>(<b>${data.length}</b>) Users</span>`
            // count = 0
            if (data.length === 0) {
                let noData = document.getElementsByClassName('no-data');
                noData[0].style.display = 'flex';
            } else {
                document.querySelector('.total-items').insertAdjacentHTML('afterbegin', count)

                data.forEach(users => {

                    let cart_list = document.getElementsByClassName('cart-list');
                    createDiv(users, cart_list)
                })
            }
        }).catch(err => {
            let ring = document.getElementsByClassName('lds-ring');
            ring[0].style.display = 'none';
            errBox = document.getElementsByClassName('error')
            errBox[0].style.display = 'flex';
            console.log(err)
        })
}

fetchData()

function createDiv(data, cartList) {
    let cart = document.createElement('div')
    cart.setAttribute('class', 'cart')

    let img = document.createElement('div')
    img.setAttribute('class', 'img')

    let image = document.createElement('img')
    image.src = "./images/blank-user.png"

    img.appendChild(image)

    let product = document.createElement('div')
    product.setAttribute('class', 'product')

    let title = document.createElement('h2')
    title.setAttribute('id', 'full-name')
    let fullName = data.name.firstname + ' ' + data.name.lastname

    title.textContent = fullName

    let description = document.createElement('p')
    description.setAttribute('id', 'address')
    let address = data.address.number + ', ' + data.address.street + ', ' + data.address.city + ', ' + data.address.zipcode + '.'
    description.textContent = address

    product.appendChild(title)
    product.appendChild(description)

    let cat = document.createElement('div')
    cat.setAttribute('class', 'description')

    let productCategory = document.createElement('h3')
    productCategory.textContent = data.username

    cat.appendChild(productCategory)
    product.appendChild(cat)

    let rating = document.createElement('div')
    rating.setAttribute('class', 'rating')
    let ratingUser = `<span><i class="fa fa-envelope"></i> ${data.email} | <i class="fa solid fa-phone"></i> ${data.phone} </span>`
    rating.insertAdjacentHTML('afterbegin', ratingUser)

    product.appendChild(rating)

    let price = document.createElement('div')
    price.setAttribute('class', 'f-rate')
    finalRate = data.id
    price.insertAdjacentHTML('afterbegin', finalRate)

    cartList[0].append(cart)
    cart.appendChild(img)
    cart.appendChild(product)
    cart.appendChild(price)

}

