const form = document.getElementById('form')
const firstName = document.getElementById('first-name')
const lastName = document.getElementById('last-name')
const email = document.getElementById('email')
const password = document.getElementById('password')
const cnfPassword = document.getElementById('cnf-password')
const checkbox = document.getElementById('checkbox')
const btnSubmit = document.getElementById('btn-submit')

console.log(form)

form.addEventListener('submit', function (e) {
    e.preventDefault()

    validateInputs()
})

function setError(element, message) {
    const inputControl = element.parentElement;
    const errDiv = inputControl.querySelector('.error')

    errDiv.textContent = message;
    element.style.border = '2px solid red'
}

function setSucess(element) {
    const inputControl = element.parentElement;
    const errDiv = inputControl.querySelector('.error')

    errDiv.textContent = '';
    element.style.border = '2px solid green'
}

function isValidEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return re.test(String(email).toLowerCase())
}

function isValidName(name) {
    const re = /^[a-zA-Z]*$/
    return re.test(String(name))
}

function validateInputs() {
    const firstNameValue = firstName.value.trim()
    const lastNameValue = lastName.value.trim()
    const emailValue = email.value.trim()
    const passwordValue = password.value.trim()
    const confirmValue = cnfPassword.value.trim()

    // console.log(firstNameValue)

    if (firstNameValue === '') {
        setError(firstName, 'First name is required')
    } else if (!isValidName(firstNameValue)) {
        setError(firstName,'Invalid name entered')
    } else {
        setSucess(firstName)
        console.log(firstName.parentElement.classList)
    }

    if (lastNameValue === '') {
        setError(lastName, 'Last name is required')
    } else if (!isValidName(lastNameValue)) {
        setError(lastName,'Invalid last name entered')
    }else {
        setSucess(lastName)
    }

    if (emailValue === '') {
        setError(email, 'Email address is required')
    } else if (!isValidEmail(emailValue)) {
        setError(email, 'Invalid email address')
    } else {
        setSucess(email)
    }

    if (passwordValue === '') {
        setError(password, 'Password is required')
    } else if (passwordValue.length < 8) {
        setError(password, 'Must have 8 characters')
    } else {
        setSucess(password)
    }

    if (confirmValue === '') {
        setError(cnfPassword, 'Confirm password is required')
    } else if (passwordValue !== confirmValue) {
        setError(cnfPassword, 'Password did not match')
    } else {
        setSucess(cnfPassword)
    }

    if (checkbox.checked !== true) {
        setError(checkbox, 'Please check the box')
    } else {
        setSucess(checkbox)
    }
}